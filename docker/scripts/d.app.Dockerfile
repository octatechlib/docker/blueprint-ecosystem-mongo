FROM node:18.14.2-bullseye as dependencies

EXPOSE 80

WORKDIR /opt/application
# RUN ls-la and return error

RUN apt-get update
RUN apt install xdg-utils --fix-missing -y

RUN chown -R node:node \
    /run \
    /opt/application

USER node
RUN mkdir -p node_modules

COPY package*.json .
#RUN npm ci

USER root

#COPY coverage.sh coverage.sh
#RUN chmod +x coverage.sh

FROM dependencies as development

#RUN sed -i "s/user = www-data/user = node/g" /usr/local/etc/php-fpm.d/www.conf
#RUN sed -i "s/group = www-data/group = node/g" /usr/local/etc/php-fpm.d/www.conf

COPY ./scripts/s.app.install.sh /tmp/docker-entrypoint-app.sh

RUN ["chown", "-R", "node:node", "/tmp/docker-entrypoint-app.sh"]
RUN ["chmod", "+x", "/tmp/docker-entrypoint-app.sh"]

USER node

ENTRYPOINT ["/tmp/docker-entrypoint-app.sh"]