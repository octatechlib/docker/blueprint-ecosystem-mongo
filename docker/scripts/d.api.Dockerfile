FROM php:8.1-fpm AS dependencies

EXPOSE 80

ARG USER_ID=1000
ARG GROUP_ID=1000

# Set working directory
WORKDIR /opt/application

RUN apt-get update \
    && apt-get install -y  \
    nginx  \
    unzip  \
    libbz2-dev  \
    libicu-dev \
     bc  \
    netcat-openbsd  \
    sudo  \
    git  \
    zlib1g-dev  \
    libpng-dev  \
    libjpeg-dev \
    libxml2-dev \
    libzip-dev \
    zip \
    libcurl4-openssl-dev \
    supervisor \
    ca-certificates \
    curl \
    unzip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install zip bcmath bz2 intl pdo_mysql opcache gd soap curl pdo_mysql mysqli zip opcache

RUN pecl install xdebug && docker-php-ext-enable xdebug

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Create a non-root user
RUN addgroup --gid $GROUP_ID www && \
    adduser --disabled-password --gecos '' --uid $USER_ID --gid $GROUP_ID www && \
    passwd -d www && \
    echo 'www ALL=(ALL:ALL) NOPASSWD: ALL' > /etc/sudoers

# Correct permissions for non-root operations
RUN chown -R www:www \
    /run \
    /var/log/supervisor \
    /var/log/nginx \
    /var/lib/nginx \
    /opt/application

RUN sed -i "s/user = www-data/user = www/g" /usr/local/etc/php-fpm.d/www.conf
RUN sed -i "s/group = www-data/group = www/g" /usr/local/etc/php-fpm.d/www.conf

COPY ./scripts/s.api.install.sh /tmp/docker-entrypoint-api.sh

RUN ["chown", "-R","www:www", "/tmp/docker-entrypoint-api.sh"]
RUN ["chmod", "+x", "/tmp/docker-entrypoint-api.sh"]

USER www

ENTRYPOINT ["/tmp/docker-entrypoint-api.sh"]
