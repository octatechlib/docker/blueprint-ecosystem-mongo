FROM php:8.1-fpm AS dependencies

EXPOSE 80

ARG USER_ID=1000
ARG GROUP_ID=1000

# Set working directory
WORKDIR /opt/application

RUN apt-get update \
    && apt-get install -y  \
    nginx  \
    unzip  \
    libbz2-dev  \
    libicu-dev \
     bc  \
    netcat-openbsd  \
    sudo  \
    git  \
    zlib1g-dev  \
    libpng-dev  \
    libjpeg-dev \
    libxml2-dev \
    libzip-dev \
    zip \
    libcurl4-openssl-dev \
    supervisor \
    ca-certificates \
    curl \
    unzip \
    && apt-get clean \`     
    && rm -rf /var/lib/apt/lists/*

FROM dependencies as development

USER node
ENTRYPOINT ["sh", "-c", "while :; do sleep 10 && echo 'API Zzzzz...'; done"]