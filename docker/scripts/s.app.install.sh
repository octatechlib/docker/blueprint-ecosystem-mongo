#!/usr/bin/env bash

set -e

ROOT_DIRECTORY=$(pwd)
WORKING_DIR=/opt/application

# shellcheck disable=SC2028
sed -i "s/#alias ll='ls -l'/alias ll='ls -l'/g" ~/.bashrc
# shellcheck disable=SC1090
source ~/.bashrc # subshell only TODO

# Start serving (this also prevents the container from exiting)
#exec /usr/bin/supervisord
#while :; do sleep 10 && echo "APP Zzzzz..."; done

if [[ -d "$WORKING_DIR" && -f "$WORKING_DIR/package.json" ]];
then
  cd ${WORKING_DIR} && npm install && npm run serve -- --port 80
else
	while :; do sleep 10 && echo "Working directory does not exist or there is no package.json." && echo "APP Zzzzz..."; done
fi