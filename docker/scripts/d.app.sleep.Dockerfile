FROM node:18.14.2-bullseye as dependencies

EXPOSE 80

WORKDIR /opt/application

FROM dependencies as development

USER node
ENTRYPOINT ["sh", "-c", "while :; do sleep 10 && echo 'APP Zzzzz...'; done"]