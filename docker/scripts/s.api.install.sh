#!/usr/bin/env bash

set -e

ROOT_DIRECTORY=$(pwd)

# shellcheck disable=SC2028
sed -i "s/#alias ll='ls -l'/alias ll='ls -l'/g" ~/.bashrc
# shellcheck disable=SC1090
source ~/.bashrc # subshell only TODO

# Start serving (this also prevents the container from exiting)
#exec /usr/bin/supervisord
while :; do sleep 10 && echo "API Zzzzz..."; done
