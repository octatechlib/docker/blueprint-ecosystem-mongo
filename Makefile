include docker/.env.docker.local

USER_UID := $(shell id -u)
USER_NAME := $(shell id -un)
USER_GID := $(shell id -g)
USER_GROUP := $(shell id -g -n)

##---------------------------------------------------------------------------- help

help:##
	@echo ""
	@echo "Commands:"
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
	@echo ""
.PHONY: help

ONESHELL:
eco-print-variables:   ## Show variables # TODO: basicAuth credentials are "admin:pass", it is recommended you change this in your config.js!
	@echo ""
	@echo "Ecosystem"
	@echo ""
	@echo "      Frontend http://$(ECO_APP_IP)"
	@echo "               -    email: $(ECO_EMAIL_USER)"
	@echo "               - password: $(ECO_EMAIL_USER_PASSWORD)"
	@echo "       Backend http://$(ECO_API_IP)"
	@echo "  MongoExpress http://$(ECO_DB_MANAGEMENT_IP):8081 (admin:pass)"
	@echo "                        - username: $(ECO_DATABASE_MAIN_USER)"
	@echo "                        - password: $(ECO_DATABASE_MAIN_USER_PASSWORD)"
	@echo "               - connection string: mongodb://$(ECO_DATABASE_ROOT_USER):$(ECO_DATABASE_ROOT_USER_PASSWORD)@$(ECO_DB_VM_IP):27017/?authSource=$(ECO_DATABASE_MAIN_AUTH_SOURCE)"
	@echo "       Mailhog http://$(ECO_DB_MAILHOG_IP):8025"
	@echo ""
	@echo "Containers"
	@echo ""
	@docker ps --format 'table {{.ID}}\t{{.Names}}'
	@echo ""
.PHONY: eco-print-variables

.ONESHELL:
eco-start:             ## Start docker containers for ecosystem development
	@echo "Starting services"
	@docker compose -f docker/docker-compose.ubuntu.yml  --env-file=docker/.env.docker.local --profile ecosystem_development start
	@make -s eco-print-variables
.PHONY: eco-start

.ONESHELL:
eco-stop:              ## Stop docker containers for ecosystem development
	@echo "Stoping services"
	@docker compose -f docker/docker-compose.ubuntu.yml  --env-file=docker/.env.docker.local --profile ecosystem_development stop
.PHONY: eco-stop

.ONESHELL:
eco-up:                ## Go up with all docker containers for ecosystem development
	@if [ ! -d "projects/api" ]; then echo "Dir api is not installed"; exit 0; fi
	@if [ ! -d "projects/app" ]; then echo "Dir app is not installed"; exit 0; fi
	@
	@echo "All services going UP"
	@USER_NAME=$(USER_NAME) USER_UID=$(USER_UID) USER_GROUP=$(USER_GROUP) USER_GID=$(USER_GID) docker compose -f docker/docker-compose.ubuntu.yml  --env-file=docker/.env.docker.local --profile=ecosystem_development up -d
	@echo "all services are UP"
	@make -s eco-print-variables
.PHONY: eco-up

.ONESHELL:
eco-down:              ## Go down with all docker containers for ecosystem development
	@echo "Services going DOWN"
	@docker compose -f docker/docker-compose.ubuntu.yml  --env-file=docker/.env.docker.local --profile ecosystem_development down
.PHONY: eco-down

.ONESHELL:
eco-down-or:           ## Go down with all docker containers for ecosystem development
	@echo "Services going DOWN with removing orphans"
	@docker compose -f docker/docker-compose.ubuntu.yml  --env-file=docker/.env.docker.local --profile ecosystem_development down --remove-orphans
.PHONY: eco-down-or

.ONESHELL:
eco-install-backend:   ## Build all services for ecosystem development backend (auth, api, storage)
	#@if [ ! -d "project/api" ]; then echo "Will clone api app" && GIT_SSH_COMMAND="ssh -i ${SSH_CERT_PATH}" git clone ${REPO_API} ${PWD}/project/api; fi TODO: load repo
	#@if [ ! -d "project/app" ]; then echo "Will clone app app" && GIT_SSH_COMMAND="ssh -i ${SSH_CERT_PATH}" git clone ${REPO_STORAGE} ${PWD}/project/app; fi TODO: load repo
	@
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose -f docker/docker-compose.ubuntu.yml  --env-file=docker/.env.docker.local --profile=ecosystem_backend up --build -d
	@
	@if [ -f "./project/api/.env" ]; then rm ./project/api/.env; fi
	@touch ./project/api/.env
	@cat core/.env.base.api >  ./project/api/.env
	@
	@echo "ECO_API_NAME=\"$(ECO_API_NAME)\"" | tee -a ./project/api/.env
	@echo "ECO_APP_IP=\"$(ECO_APP_IP)\"" | tee -a ./project/api/.env
	@echo "ECO_API_NAME=\"$(ECO_API_NAME)\"" | tee -a ./project/api/.env
	@echo "" | tee -a ./project/api/.env
	@echo "ECO_DB_VM_IP=\"$(ECO_DB_VM_IP)\"" | tee -a ./project/api/.env
	@echo "ECO_DATABASE_MAIN=\"$(ECO_DATABASE_MAIN)\"" | tee -a ./project/api/.env
	@echo "ECO_DATABASE_MAIN_USER=\"$(ECO_DATABASE_MAIN_USER)\"" | tee -a ./project/api/.env
	@echo "ECO_DATABASE_MAIN_USER_PASSWORD=\"$(ECO_DATABASE_MAIN_USER_PASSWORD)\"" | tee -a ./project/api/.env
	@echo "" | tee -a ./project/api/.env
	@echo "ECO_DB_MAILHOG_IP=\"$(ECO_DB_MAILHOG_IP)\"" | tee -a ./project/api/.env
	@echo "" | tee -a ./project/api/.env
	@
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose -f docker/docker-compose.ubuntu.yml  --env-file=docker/.env.docker.local exec eco_col_api composer install
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose -f docker/docker-compose.ubuntu.yml  --env-file=docker/.env.docker.local exec eco_col_api composer post-install-cmd
	@
	@
	@make -s eco-print-variables
.PHONY: eco-install-backend

.ONESHELL:
eco-install-app:       ## Build app services for ecosystem development app
	#@if [ ! -d "project/app" ]; then echo "Will clone app app" && GIT_SSH_COMMAND="ssh -i ${SSH_CERT_PATH}" git clone ${REPO_APP} ${PWD}/project/app; fi TODO: load repo
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose -f docker/docker-compose.ubuntu.yml  --env-file=docker/.env.docker.local up ecosystem_app --build
	@
	@if [ -f "./project/app/.env" ]; then rm ./project/app/.env; fi
	@touch ./project/app/.env
	@cat core/.env.base.app >  ./project/app/.env
	@
	@echo "ECO_APP_IP=\"$(ECO_APP_IP)\"" | tee -a ./project/app/.env
	@echo "" | tee -a ./project/app/.env
	@echo "ECO_API_NAME=\"$(ECO_API_NAME)\"" | tee -a ./project/app/.env
	@echo "ECO_API_NAME=\"$(ECO_API_NAME)\"" | tee -a ./project/app/.env
	@echo "" | tee -a ./project/app/.env
	@echo "ECO_DB_VM_IP=\"$(ECO_DB_VM_IP)\"" | tee -a ./project/app/.env
	@echo "ECO_DATABASE_MAIN=\"$(ECO_DATABASE_MAIN)\"" | tee -a ./project/app/.env
	@echo "ECO_DATABASE_MAIN_USER=\"$(ECO_DATABASE_MAIN_USER)\"" | tee -a ./project/app/.env
	@echo "ECO_DATABASE_MAIN_USER_PASSWORD=\"$(ECO_DATABASE_MAIN_USER_PASSWORD)\"" | tee -a ./project/app/.env
	@echo "" | tee -a ./project/app/.env
	@echo "ECO_DB_MAILHOG_IP=\"$(ECO_DB_MAILHOG_IP)\"" | tee -a ./project/app/.env
	@echo "ECO_AUTH_SERVICE_NAME=\"$(ECO_AUTH_SERVICE_NAME)\"" | tee -a ./project/app/.env
	@echo "" | tee -a ./project/app/.env
	@
.PHONY: eco-install-app

.ONESHELL:
eco-build:             ## Docker build no cache
	@echo "Docker build no cache"
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose -f docker/docker-compose.ubuntu.yml  --env-file=docker/.env.docker.local build --no-cache
.PHONY: eco-build

.ONESHELL:
eco-logs:              ## Show logs
	@echo "Docker LOGS"
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose -f docker/docker-compose.ubuntu.yml  --env-file=docker/.env.docker.local logs
.PHONY: eco-logs

.ONESHELL:
eco-hard-prune:        ## Hard prune everything
	@echo "Docker Hard Prune"
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker network prune && docker system prune -a --volumes -f && docker volume rm $(docker volume ls -q)
.PHONY: eco-logs

##
##----------------------------------------------------------------- api service

.ONESHELL:
eco-logs-api_service: ## Show logs for api service
	@echo "Docker LOGS"
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker compose -f docker/docker-compose.ubuntu.yml  --env-file=docker/.env.docker.local logs eco_col_api --follow
.PHONY: eco-logs-api_service

##----------------------------------------------------------------- app service

.ONESHELL:
eco-logs-app_service: ## Show logs for app service
	@echo "Docker LOGS"
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker ps -aq | xargs docker stop && docker compose -f docker/docker-compose.ubuntu.yml --env-file=docker/.env.docker.local logs eco_col_app --follow > /dev/null
.PHONY: eco-logs-app_service

##----------------------------------------------------------------- db management service

.ONESHELL:
eco-logs-app_service: ## Show logs for db management (mongo-express)
	@echo "Docker LOGS"
	@USER_UID=$(USER_UID) USER_GID=$(USER_GID) docker logs eco_col_db_management --follow
.PHONY: eco-logs-app_service


